package Method;
//STEP 1

import java.util.Scanner;

public class ScannerDemo1 {
    public static void main(String[] args) {
        //STEP 2
        Scanner sc1=new Scanner(System.in);
        //STEP 3
        System.out.println("ENTER USEERNAME");
        String s=sc1.next();
        System.out.println("HELLO......."+s);
    }
}
