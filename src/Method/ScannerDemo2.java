package Method;

import java.util.Scanner;

public class ScannerDemo2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER FIRST NUMBER");
        double num1= sc1.nextDouble();
        System.out.println("ENTER SECOND NUMBER");
        double num2= sc1.nextDouble();
        addition(num1,num2);
        substraction(num1,num2);
        multiplication(num1,num2);
        division(num1,num2);
    }
    static void addition(double n1,double n2)
    {
        double r=n1+n2;
        System.out.println("ADDITION:"+r);
    }
    static void substraction(double n1,double n2)
    {
        double r=n1-n2;
        System.out.println("SUBSTRACTION:"+r);
    }
    static void multiplication(double n1,double n2)
    {
        double r=n1*n2;
        System.out.println("MULTIPICATION:"+r);
    }
    static void division(double n1,double n2)
    {
        double r=n1/n2;
        System.out.println("DIVISION:"+r);
    }

}
