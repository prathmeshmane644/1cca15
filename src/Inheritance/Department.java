package Inheritance;

public class Department extends college {
    public Department(String universityName,String collegeName,String departmentName){
        super(universityName,collegeName);
        System.out.println("DEPARTMENT:"+departmentName);
    }
}
