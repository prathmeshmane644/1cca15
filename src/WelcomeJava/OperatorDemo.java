package WelcomeJava;

public class OperatorDemo {
    public static void main(String[] args) {
        System.out.println(10+5);
        System.out.println(12.25+36.78);
        System.out.println(10+35.25);
        System.out.println(10+"JAVA");
        System.out.println(10+5+"SQL");
        System.out.println("JAVA"+10+20);
        System.out.println("JAVA"+(10+20));
        System.out.println("A"+"B");
        System.out.println("A"+25);
        System.out.println('C'+"JAVA");
        System.out.println("CORE"+"JAVA");
    }
}
